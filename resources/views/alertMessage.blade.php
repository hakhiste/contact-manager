<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        @if ($message =  \Illuminate\Support\Facades\Session::get('success') )
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src='{{ asset("assets/frontend/images/pop-close.png") }}'>
                </button> -->
                {{ $message }}
            </div>
        @endif

        @if ($message = \Illuminate\Support\Facades\Session::get('error'))
            <div class="alert alert-danger alert-block">
                {{ $message }}
                <!-- <button type="button" class="close" data-dismiss="alert">×</button>
                <strong></strong> -->
            </div>
        @endif

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </div>
</div>