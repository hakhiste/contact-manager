<!DOCTYPE html>
<html>
    <head>
        <title>Contact Manager</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/signin.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900" rel="stylesheet"> 
        <!-- end of global css -->
        
        <!--page level css-->
        @yield('header_styles')
        <!--end of page level css-->
    </head>
    <body>
    <div class="container">
        <h1 class="text-center">Contact Manager</h1>
<form class="form-signin" action="{{route('login')}}" method="POST">
{{ csrf_field() }}

  <h3 class="form-signin-heading">Sign in</h3>
  @include('alertMessage')
  <label for="email" class="sr-only">Email address</label>
  <input type="email" name="email"  id="email" class="form-control" placeholder="Email address" required autofocus>
  <label for="password" class="sr-only">Password</label>
  <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
  <!-- <div class="checkbox">
    <label>
      <input type="checkbox" value="remember-me"> Remember me
    </label>
  </div> -->
  <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
   
  <a href="register" class="btn btn-lg btn-default btn-block" >Create Account</a>
</form>

</div> <!-- /container -->
    </body>
</html>
