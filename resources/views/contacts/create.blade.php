@extends('layouts.app')

@section('header_styles')
@stop

@section('content')
<div class="container theme-showcase" role="main">
      <div class="page-header">
        <h3 class="form-signin-heading">@if(empty($contact['id'])) Create @else Edit @endif Contact</h3>
      </div>

      @include('alertMessage')
      
      {!! Form::model($contact, ['route' => 'contact.store','id' => 'form', 'class'=>'form-signin', 'method'=> 'POST', 'files' => true]) !!}
        {{ csrf_field() }}
        {!! Form::hidden('id', null) !!}
        {!! Form::hidden('created_by', \Auth::id()) !!}
        <div class="row">
          <div class="form-group col-md-4">
            <label for="first_name" class="">First Name*</label>
            {!! Form::text('first_name',null,['class' => 'form-control','placeholder' => 'Enter First Name', 'id' => 'first_name', 'required'=>'required', 'autofocus' => 'autofocus']) !!}
          </div>
          <div class="form-group col-md-4">
            <label for="middle_name" class="">Middle Name</label>
            {!! Form::text('middle_name',null,['class' => 'form-control','placeholder' => 'Enter Middle Name', 'id' => 'middle_name']) !!}
          </div>
          <div class="form-group col-md-4">
            <label for="last_name" class="">Last Name*</label>
            {!! Form::text('last_name',null,['class' => 'form-control','placeholder' => 'Enter Last Name', 'id' => 'last_name', 'required'=>'required']) !!}
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-4">
            <label for="phone_primary" class="">Primary Phone*</label>
            {!! Form::text('phone_primary',null,['class' => 'form-control','placeholder' => 'Enter Primary Phone', 'id' => 'phone_primary', 'required'=>'required']) !!}
          </div>
          <div class="form-group col-md-4">
            <label for="phone_secondary" class="">Secondary Phone</label>
            {!! Form::text('phone_secondary',null,['class' => 'form-control','placeholder' => 'Enter Secondary Phone', 'id' => 'phone_secondary']) !!}
          </div>
          <div class="form-group col-md-4">
            <label for="email" class="">Email Address*</label>
            {!! Form::email('email',null,['class' => 'form-control','placeholder' => 'Enter Email', 'id' => 'email', 'required'=>'required']) !!}
          </div>
        </div>
        
        <div class="row">
          <div class="form-group col-md-6">
            <label>Contact image/photo</label>
            <div class="row">
              <div class="col-md-3">
                @if(empty($contact->id))
                  <img class="img-thumbnail" alt="200x200" data-holder-rendered="true" src="{{asset(config('constants.DEFAULT_PROFILE_IMAGE'))}}" />
                @else
                  @if(empty($contact->profile_image))
                      <img class="img-thumbnail" alt="200x200" data-holder-rendered="true" src="{{asset(config('constants.DEFAULT_PROFILE_IMAGE'))}}" />
                  @else
                      <img class="img-thumbnail" alt="200x200" data-holder-rendered="true" src="{{asset($contact->profile_image)}}" />
                  @endif
                @endif
              </div>
              <div class="col-md-9">
              <input name="profile_image" type="file" id="profile_image">
              <p class="text-info">
                Only jpeg,jpg,png,gif files are allowed. <br>Max file size should be 10MB.
              </p>
              </div>
            </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 float-right text-right">
              <button class="btn btn-sm btn-primary" type="submit">{{ (empty($contact->id) ? 'Create' : 'Update')}} Contact</button>
              <a href="{{url()->previous()}}" class="btn btn-sm btn-default" >Cancel</a>
            </div>
        </div>
    
        

      {!! Form::close() !!}
      </div>



    </div> <!-- /container -->

@stop

@section('footer_scripts')
@stop