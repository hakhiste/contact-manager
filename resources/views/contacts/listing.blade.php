@if($contacts->count() > 0)
      <div class="row">
        @foreach($contacts as $contact)
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{$contact->first_name}} {{$contact->middle_name}} {{$contact->last_name}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                @if(empty($contact->profile_image))
                                    <img class="img-thumbnail" alt="200x200" data-holder-rendered="true" src="{{asset(config('constants.DEFAULT_PROFILE_IMAGE'))}}" />
                                @else
                                    <img class="img-thumbnail" alt="200x200" data-holder-rendered="true" src="{{asset($contact->profile_image)}}" />
                                @endif
                            </div>
                            <div class="col-md-8" style="padding-left:0">
                                <p>
                                    <label>Primary: </label> {{$contact->phone_primary}}
                                    <br>
                                    <label>Secondary: </label> {{$contact->phone_secondary}}
                                    <br>
                                    <label>Email: </label> 
                                    {{(strlen($contact->email)>20) ? substr($contact->email, 0, 20).'...' : $contact->email}}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <a href="javascript:;" class="pull-left">0 shares</a>
                        <a href="{{route('contact.edit', [encrypt($contact->id)])}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                        <a href="#" class="btn btn-xs btn-default"><i class="fa fa-share-alt"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-danger deleteContact" data-href="{{route('contact.delete', [$contact->id]) }}"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
        
      </div>
    @else
        <div class="row">
            <div class="col-md-12 text-center">
            <div class="panel panel-default" style="padding: 20px 0;">
                <i class="fa fa-bell fa-3x"></i><br>
                
                <h3>No contacts found. </h3>
                <a href="{{url('contact/create')}}" class="btn btn-link" style="font-size:16px;">Add New Contact</a>
                </div>
            </div>
        </div>
    @endif