@extends('layouts.app')

@section('header_styles')
@stop

@section('content')

<div class="container theme-showcase" role="main">
    <div class="page-header">
    <div class="row">
        <div class="col-md-5"><h1 style="margin:0">Contacts</h1></div>
        {!! Form::model([], ['route' => 'contact.search', 'id' => 'searchForm', 'class'=>'form-signin', 'method'=> 'GET']) !!}
            <div class="col-md-3">
                <input type="text" class="form-control" placeholder="Search" name="search" id="search" required>
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary" type="submit" title="Search"><i class="fa fa-search"></i></button>
                <button class="btn btn-default" type="button" id="resetSearch" title="Reset"><i class="fa fa-times"></i></button>
            </div>
        {!! Form::close() !!}
        <div class="col-md-2">
            <a href="{{url('contact/create')}}" class="btn btn-primary pull-right">Add New Contact</a>
        </div>
    </div>

    </div>
    @include('alertMessage')

    <div id="contactContent">Loading...</div>
    </div> <!-- /container -->

<div class="modal fade" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Contact</h4>
      </div>
      <div class="modal-body">
        <p>This will delete contact permanantly. Are you sure about this ?</p>
      </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <a href="#" class="btn btn-danger" id="confirm">Delete</a>
        </div>
    </div>
  </div>
</div>
@stop

@section('footer_scripts')
<script>
$(document).on('ready', function() {
    loadContacts();
});

//delete contact
$(document).on('click', ".deleteContact", function(){
    $('#confirmDelete #confirm').attr('href', $(this).data('href'));
    $('#confirmDelete').modal('show');
});

//search contacts
$('#searchForm').on('submit', function(e){
    e.preventDefault();
    loadContacts();
});

//reset search
$('#resetSearch').on('click', function(){
    $('#search').val('');
    loadContacts();
});

//load contacts using ajax
function loadContacts(){
    $.ajax({
        type: "GET",
        url: $('#searchForm').attr('action'),
        data: {search: $('#search').val() },
        dataType: 'html',
        beforeSend: function(){
            $('#contactContent').html('Loading...');
        },
        success: function(data){
            $('#contactContent').html(data);
        },
    });
}
</script>
@stop
