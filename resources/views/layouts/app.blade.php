<!DOCTYPE html>
<html>
    <head>
        <title>
            {{config('constants.APP_NAME')}}
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/theme.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <!-- end of global css -->

        <!--page level css-->
        @yield('header_styles')
        <!--end of page level css-->

        <meta name="_token" content="{{ csrf_token() }}">
    </head>
    <body>
        @include('layouts._header')
        @yield('content')

        <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script>
            var APP_PATH = "{{url('/')}}";
        </script>

        <!-- begin page level js -->
        @yield('footer_scripts')
        <!-- end page level js -->
    </body>
</html>
