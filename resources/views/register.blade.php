<!DOCTYPE html>
<html>
    <head>
        <title>Contact Manager</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/signin.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900" rel="stylesheet"> 
        <!-- end of global css -->
        
        <!--page level css-->
        @yield('header_styles')
        <!--end of page level css-->
    </head>
    <body>
    <div class="container">
        <h1 class="text-center">Contact Manager</h1>
        {!! Form::model([],['route' => 'register','id' => 'form', 'class'=>'form-signin', 'method'=> 'POST']) !!}

{{ csrf_field() }}

  <h3 class="form-signin-heading">Create Account</h3>
  @include('alertMessage')
  <div class="form-group">
  <label for="name" class="sr-only">Name*</label>
  {!! Form::text('name',null,['class' => 'form-control','placeholder' => 'Enter Name', 'id' => 'name', 'required'=>'required', 'autofocus' => 'autofocus']) !!}
  </div>
  <div class="form-group">
  <label for="email" class="sr-only">Email address*</label>
  {!! Form::email('email',null,['class' => 'form-control','placeholder' => 'Enter Email', 'id' => 'email', 'required'=>'required']) !!}
  </div>
  <label for="password" class="sr-only">Password*</label>
  <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
  <label for="password" class="sr-only">Confirm Password*</label>
  <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password" required>
  <button class="btn btn-lg btn-primary btn-block" type="submit">Create Account</button>
   
  <a href="login" class="btn btn-lg btn-default btn-block" >Sign in</a>
  {!! Form::close() !!}

</div> <!-- /container -->
    </body>
</html>
