@component('mail::message')
Hi,

Thank you for registering with us.

Please verify account your account.

@component('mail::button', ['url' => url('verify_account/'.$token) ])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
