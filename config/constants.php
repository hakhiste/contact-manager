<?php


return [
    'APP_NAME' => env('APP_NAME', 'Contact Manager'),
    'DEFAULT_PROFILE_IMAGE' => 'assets/images/user-dummy.jpg',
];