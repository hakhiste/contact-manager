<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login', 'HomeController@index')->name('login');
Route::post('login', 'HomeController@doLogin')->name('login');
Route::get('logout', 'HomeController@logout')->name('logout');
Route::get('register', 'HomeController@register')->name('register');
Route::post('register', 'HomeController@doRegister')->name('register');
Route::get('verify_account/{token}', 'HomeController@verifyAccount')->name('verify_account');

Route::group(["middleware" => "CheckLogin"], function (){
    Route::get('/', 'ContactController@index')->name('home');
    Route::get('delete/{id}', 'ContactController@destroy')->name('contact.delete');
    Route::resource('contact', 'ContactController');

    Route::get('search', 'ContactController@search')->name('contact.search');
});
