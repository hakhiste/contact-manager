<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\ContactManager\User\UserInterface;
use App\ContactManager\User\UserRepository;

use App\ContactManager\Contact\ContactInterface;
use App\ContactManager\Contact\ContactRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserInterface::class, UserRepository::class);
        $this->app->bind(ContactInterface::class, ContactRepository::class);
    }

    public function provides()
    {
        return [
            UserInterface::class,
            ContactInterface::class,
        ];
    }
}
