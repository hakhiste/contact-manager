<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShareContact extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'contact_id', 'shared_with'
    ];

    /**
     * relationship with table user
     *
     */
    public function user() {
        return $this->belongsTo(User::class, 'id', 'shared_with');
    }

    /**
     * relationship with table user
     *
     */
    public function contact() {
        return $this->belongsTo(Contact::class, 'id', 'contact_id');
    }
}
