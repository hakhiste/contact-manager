<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'created_by', 'first_name', 'middle_name', 'last_name', 'email', 'phone_primary', 'phone_secondary', 'profile_image'
    ];

    /**
     * relationship with table user
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'created_by');
    }

    /**
     * relationship with table user
     *
     */
    public function shared() {
        return $this->hasMany(ShareContact::class, 'contact_id', 'id');
    }
}
