<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'status', 'remember_token'
    ];

    /**
     * relationship with table contact
     *
     */
    public function contacts()
    {
        return $this->hasMany(Contact::class, 'created_by', 'id');
    }
}
