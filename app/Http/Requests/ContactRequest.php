<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'email' => 'required|email|max:150',
            'phone_primary' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:20',
            'phone_secondary' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:20',
            'profile_image' => 'nullable|mimes:jpeg,jpg,png,gif|max:10000',
        ];
    }
}
