<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;

use App\ContactManager\Contact\ContactInterface;
use Auth;

class ContactController extends Controller
{
    public $repoContact;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ContactInterface $repoContact) {
        $this->repoContact = $repoContact;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('welcome');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        $contacts = $this->repoContact->getContacts(Auth::id(), $request->search)->sortBy('first_name');
        return view('contacts.listing', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $contact = null;
        return view('contacts.create', compact('contact'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ContactRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request, ContactInterface $repoContact) {
        if( $repoContact->saveContact($request) ) {
            return redirect()->route('home')->with('success', 'Contact '.((empty($request->id)) ? 'created' : 'updated').' successfully');
        } else {
            return redirect()->back()->with('error', 'Error while saving contact. Try again later.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $id = decrypt($id);
            $contact = $this->repoContact->findContact($id);
            return view('contacts.create', compact('contact'));
        } catch(\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if($this->repoContact->delete($id)) {
            return redirect()->back()->with('success', 'Contact deleted successfully!');
        } else {
            return redirect()->back()->with('error', 'Error while deleting contact. Please try again.');
        }
    }
}
