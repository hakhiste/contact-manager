<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\{LoginRequest, RegisterRequest};

use App\ContactManager\User\UserInterface;
use Auth;

class HomeController extends Controller
{
    private $repoUser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserInterface $repoUser) {
        $this->repoUser = $repoUser;
    }

    /**
     * Show the application login.
     *
     * @return view
     */
    public function index() {
        if(Auth::check()) {
            return redirect()->route('home');
        }
        return view('login');
    }
    
    /**
     * Show the application login.
     *
     * @return \Illuminate\Http\Response
     */
    public function doLogin(LoginRequest $request) {
        $result = $this->repoUser->doLogin($request);
        if($result) {
            return redirect()->to(route('home'));
        } else {
            return redirect()->back()->with('error', 'Email or password is incorrect. Or your account may be inactive.');
        }
    }

    /**
     * Show the application registration.
     *
     * @return view
     */
    public function register() {
        if(Auth::check()) {
            return redirect()->route('home');
        }
        return view('register');
    }
    
    /**
     * Register new user
     *
     * @return \Illuminate\Http\Response
     */
    public function doRegister(RegisterRequest $request) {
        $result = $this->repoUser->doRegister($request);
        
        if($result) {
            return redirect()->route('login')->with('success', 'You are registered successfully! Please verify your email account.');
        } else {
            return redirect()->back()->with('error', 'Registration failed. Try again.');
        }
    }
    
    /**
     * Logout user from application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {
        Auth::logout();
        return redirect()->to('login');
    }
    
    /**
     * Logout user from application.
     *
     * @return \Illuminate\Http\Response
     */
    public function verifyAccount($token) {
        if($this->repoUser->verifyAccount($token)) {
            return redirect()->to('login')->with('success', 'Account verified successfully! Login with your credentials');
        } else {
            return redirect()->to('login')->with('error', 'Token is invalid or expired.');
        }
        
    }
}
