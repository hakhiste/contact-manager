<?php
namespace App\ContactManager\User;


interface UserInterface
{
    public function doLogin($request);
    public function doRegister($request);
    public function verifyAccount($remember_token);
    public function generateRandomString($length);
}