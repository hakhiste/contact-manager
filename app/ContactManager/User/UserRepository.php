<?php
namespace App\ContactManager\User;


use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Mail\Register;

class UserRepository implements UserInterface
{
    private $user;

    public function __construct(User $user) {
        $this->user = $user;
    }

    /**
    * Update existing password from user table.
    *
    * @param request
    *
    * @return bool
    */
    public function doLogin($request) {
        $credentials = $request->only('email', 'password');

        if (\Auth::attempt($credentials + ['status' => 'active'])) {
            return true;
        }
        return false;
    }
    
    /**
    * Save new registration in Users table and send verification link on email.
    *
    * @param request
    *
    * @return bool
    */
    public function doRegister($request) {
        $user = new $this->user($request->all());
        $token = $this->generateRandomString('20');
        $user->password = Hash::make($request->password);
        $user->status = 'unverified';
        $user->remember_token = $token;
        if($user->save()) {
            \Mail::to($user)->send(new Register($token));
            return true;
        }
        return false;
    }
    
    /**
    * Verify token of new user and the change status to activate account.
    *
    * @param token string
    *
    * @return bool
    */
    public function verifyAccount($token) { 
        $verify = $this->user->where('remember_token', $token)->first();

        if ( !empty($verify) ) {
            $verify->status = 'active';
            $verify->remember_token = null;
            if ($verify->save()) {
                return true;
            }
        }
        return false;
    }

    /**
    * generate random string
    *
    * @param int length
    * 
    * @return string
    */
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}

          