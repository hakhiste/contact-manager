<?php
namespace App\ContactManager\Contact;


interface ContactInterface
{
    public function getContacts($userId, $search);
    public function saveContact($input);
    public function findContact($id);
    public function doUploadProfileImage($request);
}