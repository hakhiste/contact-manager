<?php
namespace App\ContactManager\Contact;


use App\Models\{User, Contact};
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use Intervention\Image\ImageManagerStatic as Image;
use File;

class ContactRepository implements ContactInterface
{
    private $user;
    private $contact;

    public function __construct(User $user, Contact $contact) {
        $this->user = $user;
        $this->contact = $contact;
    }

    /**
    * Get all active contacts of user.
    *
    * @param int user id
    *
    * @return collection
    */
    public function getContacts($userId, $search) {
        $contacts = $this->contact->where('created_by', $userId);
        if( !empty($search) ) {
            $contacts->where(function($q) use($search) {
                return $q->orWhere('first_name', 'like', '%'.$search.'%')
                        ->orWhere('last_name', 'like', '%'.$search.'%')
                        ->orWhere('phone_primary', 'like', '%'.$search.'%')
                        ->orWhere('phone_secondary', 'like', '%'.$search.'%')
                        ->orWhere('email', 'like', '%'.$search.'%');
            });
        }
        return $contacts->get();
    }

    /**
    * find Contact
    *
    * @param int contact id
    *
    * @return collection
    */
    public function findContact($id) {
        return $this->contact->findOrFail($id);
    }
    
    /**
    * Save new Contact
    *
    */
    public function saveContact($request) {
        $contact = new $this->contact();
        if(!empty($request->id)) {
            $contact = $this->findContact($request->id);
        }

        $contact->created_by    = $request->created_by;
        $contact->first_name    = $request->first_name;
        $contact->middle_name   = $request->middle_name;
        $contact->last_name     = $request->last_name;
        $contact->email         = $request->email;
        $contact->phone_primary = $request->phone_primary;
        $contact->phone_secondary = $request->phone_secondary;
        
        if($contact->save()) {
            $request->id = $contact->id;
            //upload profile image
            if(!empty($request->file('profile_image'))) {
                if(!$this->doUploadProfileImage($request)) {
                    return false;
                }
            }
            return true;
        }
        return false;
        
    }
    
    /**
    * upload contact profile image
    *
    * @param request 
    *
    * @return bool
    */
    public function doUploadProfileImage($request) {
        try{
            $image =  $request->file('profile_image');
            $name = $request->id.'.'.$image->getClientOriginalExtension();
            
            //upload image in thumbnail
            $destinationPath = public_path('uploads/contacts/');
            
            //check folder if not exist then create new folder
            if(!File::isDirectory($destinationPath)) {
                File::makeDirectory($destinationPath,0777,true);
            }
            
            $img = Image::make($image->getRealPath());
            
            $img->resize(64, 64, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$name); 

            //save image in database with random name
            $contact = $this->findContact($request->id);
            $contact->profile_image = 'uploads/contacts/'.$name;
            return $contact->save();
        } catch (\Exception $e) {
            return false;
        }
    }
    
    /**
    * delete contact from application
    *
    * @param request 
    *
    * @return bool
    */
    public function delete($id) {
        try{
            $contact = $this->contact->findOrFail($id);
            
            //delete profile image from disk
            if (!empty($contact->profile_image) && File::exists(public_path($contact->profile_image))) { 
                unlink(public_path($contact->profile_image));
            }    
            return $contact->delete();
        } catch (\Exception $e) {
            return false;
        }
    }
}     